import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DashboardContainerComponent } from './modules/dashboard/components/dashboard-container/dashboard-container.component';
import { ProjectContainerComponent } from './modules/projects/project-container/project-container.component';

@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: 'dashboard', component: DashboardContainerComponent },
            { path: 'projects', component: ProjectContainerComponent },
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },            
        ])
    ],    
    exports: [ RouterModule ]
})
export class AppRoutingModule { }