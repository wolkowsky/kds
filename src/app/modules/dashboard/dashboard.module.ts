import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardContainerComponent } from './components/dashboard-container/dashboard-container.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [DashboardContainerComponent],
  exports: [DashboardContainerComponent]
})
export class DashboardModule { }
