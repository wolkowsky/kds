import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectContainerComponent } from './project-container/project-container.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ProjectContainerComponent],
  exports: [ProjectContainerComponent]
})
export class ProjectsModule { }
